<?php

namespace Drupal\blinknet\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Button' Block.
 *
 * @Block(
 *   id = "blinknet_subscription_button",
 *   admin_label = @Translation("Blink.net: Subscription (popup) button"),
 *   category = @Translation("Blink.net"),
 * )
 */
class SubscriptionButton extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'button' => [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#value' => $this->t('Subscribe'),
        '#attributes' => [
          'onclick' => 'window.blinkSDK.promptSubscriptionPopup()',
          'class' => ['blink-subscription-button'],
        ],
      ],
      '#attached' => [
        'library' => [
          'blinknet/' . \Drupal::config('blinknet.settings')->get('mode'),
        ],
      ],
    ];
  }

}
