<?php

namespace Drupal\blinknet\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Button' Block.
 *
 * @Block(
 *   id = "blinknet_donation_container",
 *   admin_label = @Translation("Blink.net: Donation container"),
 *   category = @Translation("Blink.net"),
 * )
 */
class DonationContainer extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'container' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => '',
        '#attributes' => [
          'class' => ['blink-donation-container'],
        ],
      ],
      '#attached' => [
        'library' => [
          'blinknet/' . \Drupal::config('blinknet.settings')->get('mode'),
        ],
      ],
    ];
  }

}
