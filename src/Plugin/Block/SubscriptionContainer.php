<?php

namespace Drupal\blinknet\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Button' Block.
 *
 * @Block(
 *   id = "blinknet_subscription_container",
 *   admin_label = @Translation("Blink.net: Subscription container"),
 *   category = @Translation("Blink.net"),
 * )
 */
class SubscriptionContainer extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'container' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => '',
        '#attributes' => [
          'class' => ['blink-subscription-container'],
        ],
      ],
      '#attached' => [
        'library' => [
          'blinknet/' . \Drupal::config('blinknet.settings')->get('mode'),
        ],
      ],
    ];
  }

}
