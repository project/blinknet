<?php

namespace Drupal\blinknet\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Button' Block.
 *
 * @Block(
 *   id = "blinknet_donation_button",
 *   admin_label = @Translation("Blink.net: Donation (popup) button"),
 *   category = @Translation("Blink.net"),
 * )
 */
class DonationButton extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'button' => [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#value' => $this->t('Donate'),
        '#attributes' => [
          'onclick' => 'window.blinkSDK.promptDonationPopup()',
          'class' => ['blink-donation-button'],
        ],
      ],
      '#attached' => [
        'library' => [
          'blinknet/' . \Drupal::config('blinknet.settings')->get('mode'),
        ],
      ],
    ];
  }

}
