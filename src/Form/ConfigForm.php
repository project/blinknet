<?php

namespace Drupal\blinknet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for the Blink.net module.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'blinknet_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'blinknet.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('blinknet.settings');

    $form['client_id'] = [
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('This is taken from the Blink.net account system. Make sure to sign up for a <a href="https://blink.net">Blink</a> account.'),
      '#type' => 'textfield',
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];

    $form['mode'] = [
      '#title' => $this->t('Site mode'),
      '#type' => 'radios',
      '#required' => TRUE,
      '#options' => [
        'demo' => $this->t('Demo mode'),
        'live' => $this->t('Live mode'),
      ],
      '#default_value' => $config->get('mode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration and update the values.
    $config = \Drupal::service('config.factory')
      ->getEditable('blinknet.settings');

    // Track if something was changed.
    $changed = FALSE;

    // Work out if the client ID changed.
    $old_client_id = $config->get('client_id');
    $new_client_id = $form_state->getValue('client_id');
    if ($old_client_id != $new_client_id) {
      $config->set('client_id', $new_client_id);
      $changed = TRUE;

      // Work out the default theme.
      $default_theme = $this->config('system.theme')->get('default');

      // Clear the libraries cache so that the client ID can be added to the
      // library URLs.
      // @todo Make sure the appropriate output caches are also changed.
      $cache = \Drupal::cache('discovery');
      $cache->delete('library_info:' . $default_theme);
      $this->messenger()->addStatus('The site will now load the new client ID.');
    }

    // Work out if the mode changed.
    $old_mode = $config->get('mode');
    $new_mode = $form_state->getValue('mode');
    if ($old_mode != $new_mode) {
      $config->set('mode', $new_mode);
      $changed = TRUE;
      // @todo Clear the block caches so that the mode can be changed.
    }

    // If something changed, save the changes.
    if ($changed) {
      $config->save();
    }

    parent::submitForm($form, $form_state);
  }

}
