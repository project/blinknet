<?php

namespace Drupal\Tests\blinknet\Functional\Form;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Drupal\blinknet\Form\ConfigForm class.
 *
 * @group Form
 */
class ConfigFormTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['blinknet'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user object for testing the admin interface.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Perform initial setup tasks that run before every test method.
   */
  public function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer blinknet',
    ]);
  }

  /**
   * Tests the \Drupal\blinknet\Form\ConfigForm class.
   */
  public function testConfigForm() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/services/blinknet');
    $this->assertSession()->statusCodeEquals(200);
    $config = $this->config('blinknet.settings');
    $this->assertSession()->fieldValueEquals(
      'client_id',
      $config->get('client_id')
    );
  }

}
