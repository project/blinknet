BlinkNet 2.0.0, 2023-08-05
--------------------------
#3296645 by Project Update Bot, DamienMcKenna: Automated Drupal 10
  compatibility fixes.


BlinkNet 2.0.0-alpha3, 2021-09-27
---------------------------------
#3239389 by DamienMcKenna: Change "id" attributes to "class".


BlinkNet 2.0.0-alpha2, 2021-09-26
---------------------------------
#3239254 by DamienMcKenna: Fix tests on 2.0.x branch.


BlinkNet 2.0.0-alpha1, 2021-09-25
---------------------------------
#3140896 by DamienMcKenna: Initial port to D8/9.
