<?php

/**
 * @file
 * Definition of the 'block' panel style.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Blink.net Content Container'),
  'description' => t('Wraps the pane in the Blink.net content container with custom options.'),
  'render pane' => 'theme_blinknet_content_container_style_render_pane',
  'settings form' => 'blinknet_content_container_style_settings_form',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_blinknet_content_container_style_render_pane($vars) {
  $settings = $vars['settings'];

  // Identify the ID for the current object.
  $id = '';

  // Get the configured price.
  $price = 100;
  if (!empty($settings['resource_price'])) {
    $price = intval($settings['resource_price']);
  }

  // Work out what value the expiration value should be set to.
  $expiration = '';

  return '<div id="blink-content" resource-id="' . $id . '" resource-price="' . $price . '" paywall-expiration="' . $expiration . '">'
    . \Drupal::service('renderer')->render($vars['content']->content)
    . '</div>';
}

/**
 * Settings form callback.
 */
function blinknet_content_container_style_settings_form($settings) {
  $form['resource_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#description' => t('The price of reading this item in local currency units, e.g. 100 = USD$1.00.'),
    '#required' => TRUE,
    '#default_value' => isset($settings['resource_price']) ? $settings['resource_price'] : 100,
  );
  return $form;
}
