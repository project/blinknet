<?php

/**
 * @file
 * Definition of the 'block' panel style.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Blink.net Donation Container'),
  'description' => t('Wraps the pane in the Blink.net donation container.'),
  'render pane' => 'blinknet_donation_container_style_render_region',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_blinknet_donation_container_style_render_region($vars) {
  return '<div id="blink-donation-container">'
    . \Drupal::service('renderer')->render($vars['content']->content)
    . '</div>';
}
