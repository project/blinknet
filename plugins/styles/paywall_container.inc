<?php

/**
 * @file
 * Definition of the 'block' panel style.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Blink.net Paywall Container'),
  'description' => t('Wraps the pane in the Blink.net paywall container.'),
  'render pane' => 'blinknet_paywall_container_style_render_region',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_blinknet_paywall_container_style_render_region($vars) {
  return '<div id="blink-container">'
    . \Drupal::service('renderer')->render($vars['content']->content)
    . '</div>';
}
