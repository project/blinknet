# Blink.net Integration

This module allows you to inject certain elements on the page for use with the
[Blink.net](https://www.blink.net) service using the [Blink public
API](https://docs.blink.net).

## Functionality

TBD.

## Configuration

TBD.

## Credits / contact

Written and maintained by [Damien McKenna](https://www.drupal.org/u/damienmckenna).

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the [project issue
queue](https://www.drupal.org/project/issues/blinknet).
